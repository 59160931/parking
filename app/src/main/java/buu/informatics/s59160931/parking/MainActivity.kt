package buu.informatics.s59160931.parking

import android.content.Context
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import buu.informatics.s59160931.parking.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val myCar1: MyCar = MyCar("","","")
    private val myCar2: MyCar = MyCar("","","")
    private val myCar3: MyCar = MyCar("","","")
    private var slot: Int = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        hideButton()
        binding.apply {
            slot1Button.setOnClickListener {
                slot = 1
                showSlot(it)
            }
            slot2Button.setOnClickListener {
                slot = 2
                showSlot(it)
            }
            slot3Button.setOnClickListener {
                slot = 3
                showSlot(it)
            }
            confirmButton.setOnClickListener {
                addCar(it)
            }
            deleteButton.setOnClickListener {
                deleteCar(it)
            }
        }
        binding.myCar1 = myCar1
        binding.myCar2 = myCar2
        binding.myCar3 = myCar3
    }

    private fun hideButton(){
        binding.apply {
            registernumberEdit.visibility = View.GONE
            brandEdit.visibility = View.GONE
            nameEdit.visibility = View.GONE
            confirmButton.visibility = View.GONE
            deleteButton.visibility = View.GONE
        }
    }

    private fun showSlot(view: View) {
        binding.apply {
            registernumberEdit.visibility = View.VISIBLE
            brandEdit.visibility = View.VISIBLE
            nameEdit.visibility = View.VISIBLE
            confirmButton.visibility = View.VISIBLE
            deleteButton.visibility = View.VISIBLE

            if (slot == 1) {
                registernumberEdit.setText(myCar1?.registernumber)
                brandEdit.setText(myCar1?.brand)
                nameEdit.setText(myCar1?.name)
                invalidateAll()
            }
            else if (slot == 2) {
                registernumberEdit.setText(myCar2?.registernumber)
                brandEdit.setText(myCar2?.brand)
                nameEdit.setText(myCar2?.name)
                invalidateAll()
            }
            else{
                registernumberEdit.setText(myCar3?.registernumber)
                brandEdit.setText(myCar3?.brand)
                nameEdit.setText(myCar3?.name)
                invalidateAll()
            }
        }
    }

    private fun addCar(view: View){
        binding.apply {
            registernumberEdit.visibility = View.GONE
            brandEdit.visibility = View.GONE
            nameEdit.visibility = View.GONE
            confirmButton.visibility = View.GONE
            deleteButton.visibility = View.GONE

            if (slot == 1) {
                myCar1?.registernumber = registernumberEdit.text.toString()
                myCar1?.brand = brandEdit.text.toString()
                myCar1?.name = nameEdit.text.toString()
                slot1Button.setText("FULL")
                slot1Button.setBackgroundColor(Color.GREEN)
                invalidateAll()
            }
            else if (slot == 2) {
                myCar2?.registernumber = registernumberEdit.text.toString()
                myCar2?.brand = brandEdit.text.toString()
                myCar2?.name = nameEdit.text.toString()
                slot2Button.setText("FULL")
                slot2Button.setBackgroundColor(Color.GREEN)
                invalidateAll()
            }
            else{
                myCar3?.registernumber = registernumberEdit.text.toString()
                myCar3?.brand = brandEdit.text.toString()
                myCar3?.name = nameEdit.text.toString()
                slot3Button.setText("FULL")
                slot3Button.setBackgroundColor(Color.GREEN)
                invalidateAll()
            }
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken,0)
        }
    }

    private fun deleteCar(view: View) {
        binding.apply {
            registernumberEdit.visibility = View.GONE
            brandEdit.visibility = View.GONE
            nameEdit.visibility = View.GONE
            confirmButton.visibility = View.GONE
            deleteButton.visibility = View.GONE

            if (slot == 1) {
                myCar1?.registernumber = ""
                myCar1?.brand = ""
                myCar1?.name = ""
                slot1Button.setText("EMPTY")
                slot1Button.setBackgroundColor(Color.YELLOW)
                invalidateAll()
            }
            else if (slot == 2) {
                myCar2?.registernumber = ""
                myCar2?.brand = ""
                myCar2?.name = ""
                slot2Button.setText("EMPTY")
                slot2Button.setBackgroundColor(Color.YELLOW)
                invalidateAll()
            }
            else{
                myCar3?.registernumber = ""
                myCar3?.brand = ""
                myCar3?.name = ""
                slot3Button.setText("EMPTY")
                slot3Button.setBackgroundColor(Color.YELLOW)
                invalidateAll()
            }
        }
    }
}
